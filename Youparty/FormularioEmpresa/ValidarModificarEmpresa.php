<?php
session_start();


include '../Data/Conexion.php';

$nombre=$_POST['nombre'];
$direccion=$_POST['direccion'];
$telefono=$_POST['telefono'];
$servicios=$_POST['servicios'];
$email=$_POST['email'];
$_SESSION['imp']=$_POST;

$errores=array();


if($nombre=="")
{
	$errores[]=true;
	$_SESSION['error1']="Debe colocar su nombre";
}

if($servicios=="")
{
	$errores[]=true;
	$_SESSION['error4']="Debe colocar el servicio que se ofrece";
}

if($direccion=="")
{
	$errores[]=true;	
	$_SESSION['error2']="Debe colocar su dirección";
}

if($telefono=="")
{
	$errores[]=true;	
	$_SESSION['error3']="Debe colocar un número telefónico";
}


if($email=="")
{
		$errores[]=true;
	$_SESSION['error5']="Debe colocar un correo electrónico";
}

if(count($errores)>0)
{
header('location:FormularioModificarEmpresa.php');	
}
else
{
	$_SESSION['exito']="El registro se ha logrado con éxito";
	unset($_SESSION['imp']);

header('location:../PaginaEmpresa/PaginaEmpresa.php');
}


?>