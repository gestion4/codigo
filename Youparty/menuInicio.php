 
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster" />

 <nav style="position:relative; height:47px; top:143px; width:100%; background-color: #f0f0f0;
	background-image: -webkit-gradient(linear,left top, left bottom,from(#fefefe), color-stop(0.5,#f0f0f0), color-stop(0.51, #e6e6e6));
	background-image: -moz-linear-gradient(#fefefe 0%, #f0f0f0 50%, #e6e6e6 51%);
	background-image: -o-linear-gradient(#fefefe 0%, #f0f0f0 50%, #e6e6e6 51%);
	background-image: -ms-linear-gradient(#fefefe 0%, #f0f0f0 50%, #e6e6e6 51%);
	background-image: linear-gradient(#fefefe 0%, #f0f0f0 50%, #e6e6e6 51%);
	
	border-right: 1px solid rgba(9, 9, 9, 0.125);
	
	/* Adding a 1px inset highlight for a more polished efect: */
	
	box-shadow: 1px -1px 0 rgba(255, 255, 255, 0.6) inset;
	-moz-box-shadow: 1px -1px 0 rgba(255, 255, 255, 0.6) inset;
	-webkit-box-shadow: 1px -1px 0 rgba(255, 255, 255, 0.6) inset;">
            <ul class="fancyNav">
                <li id="home"><a href="../index.php" class="homeIcon">Prinicipal</a></li>
                <li id="news"><a href="../sobreNosotros.php">Sobre Nosotros</a></li>
                <li id="about"><a href="../ampliaEmpresa.php">Amplia tu empresa</a></li>
                <li id="contact" style="float:right;"><a href="../iniciarSession.php">Iniciar Sesión</a></li>
            </ul>
        </nav>